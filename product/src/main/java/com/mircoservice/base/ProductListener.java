package com.mircoservice.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.messaging.handler.annotation.SendTo;

import com.mircoservice.bean.Order;

@EnableBinding(Processor.class)
public class ProductListener {
	
	private Log logger = LogFactory.getLog(ProductListener.class);

	@StreamListener(Processor.INPUT)
	@SendTo(Processor.OUTPUT)
	public Order pack(Order order){
		String status = order.getStatus();
		order.setStatus(status + " packing");
		logger.info("Packing product: " + order);
		return order;
	}
}