package com.mircoservice.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

import com.mircoservice.bean.Order;

@EnableBinding(Sink.class)
public class ShipListener {
	
	private Log logger = LogFactory.getLog(ShipListener.class);
	
	@StreamListener(Sink.INPUT)
	public void ship(Order order) {
		String status = order.getStatus();
		order.setStatus(status + " ship");
		LocalStorage.orderMap.put(order.getSerialNum(), order);
		logger.info("shipping product: " + order);
	}
}
