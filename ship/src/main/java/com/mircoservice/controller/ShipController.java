package com.mircoservice.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mircoservice.base.LocalStorage;
import com.mircoservice.bean.Order;

@RestController
public class ShipController {
	@RequestMapping(value = "/findOrder", method = RequestMethod.GET)
	public Order findOrder(@RequestParam("orderNum") int orderNum) {
		return LocalStorage.orderMap.get(orderNum);
	}
}