package com.mircoservice.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Aspect
@Configuration
public class ServiceAspect {

	private static final Logger logger = LogManager.getLogger(ServiceAspect.class);

	private static final ObjectMapper mapper = new ObjectMapper();

	private static final String pointCut = "execution(* com.mircoservice.controller.*.*(..))";

	static {
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}

	@AfterReturning(value = pointCut, returning = "result")
	public void serviceAfterReturn(JoinPoint joinPoint, Object result) throws Exception {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		HttpSession session = request.getSession();
		String className = joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();

		List<Object> argsList = new ArrayList<>();
		for (int i = 0; i < joinPoint.getArgs().length; i++) {
			argsList.add(joinPoint.getArgs()[i]);
		}

		Map<String, Object> data = new HashMap<>();
		data.put("class", className);
		data.put("input", argsList);
		data.put("output", result);

		data.put("session", session.getId());
		data.put("request_session", request.getRequestedSessionId());
		data.put("session_created", session.getCreationTime());
		data.put("session_lastAccessed", session.getLastAccessedTime());

		data.put("protocol", request.getProtocol());
		data.put("scheme", request.getScheme());
		data.put("server_port", request.getServerPort());
		data.put("server_name", request.getServerName());
		data.put("server_info", request.getServletContext().getServerInfo());

		data.put("accept", request.getHeader("Accept"));
		data.put("host", request.getHeader("Host"));
		data.put("accept_encoding", request.getHeader("Accept-Encoding"));
		data.put("user_agent", request.getHeader("User-Agent"));
		data.put("connection", request.getHeader("Connection"));
		data.put("cookie", request.getHeader("Cookie"));

		data.put("http_method", request.getMethod());
		data.put("path_info", request.getPathInfo());
		data.put("request_url", request.getRequestURL());
		data.put("request_uri", request.getRequestURI());
		data.put("servlet_path", request.getServletPath());
		data.put("query_string", request.getQueryString());

		data.put("remote_addr", request.getRemoteAddr());
		data.put("remote_host", request.getRemoteHost());
		data.put("character_encoding", request.getCharacterEncoding());
		data.put("content_length", request.getContentLength());
		data.put("content_type", request.getContentType());

		data.values().removeIf(Objects::isNull);

		logger.info(mapper.writeValueAsString(data));
	}
}
