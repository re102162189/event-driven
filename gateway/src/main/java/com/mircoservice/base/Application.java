package com.mircoservice.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableZuulProxy
@ComponentScan({ "com.mircoservice.*" })
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}