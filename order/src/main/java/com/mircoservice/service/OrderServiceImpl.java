package com.mircoservice.service;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Service;

import com.mircoservice.bean.Order;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

	private final static AtomicInteger orderNum = new AtomicInteger(0);

	public Order process(String product) {
		Order order = new Order();
		order.setSerialNum(orderNum.incrementAndGet());
		order.setProduct(product);
		order.setStatus("order");
		return order;
	}
}