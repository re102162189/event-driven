package com.mircoservice.service;

import com.mircoservice.bean.Order;

public interface OrderService {
	Order process(String product);
}