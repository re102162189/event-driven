package com.mircoservice.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mircoservice.bean.Order;
import com.mircoservice.service.OrderService;

@RestController
@EnableBinding(Source.class)
public class OrderController {

	private Log logger = LogFactory.getLog(OrderController.class);
	
	@Autowired
	private Source source;
	
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/buy", method = RequestMethod.GET)
	public String buy(@RequestParam("product") String product) {
		
		if(product == null || product.trim().isEmpty()) {
			return "empty order";
		}
		
		Order order = orderService.process(product);		
		source.output().send(MessageBuilder.withPayload(order).build());
		logger.info("Ordering product: " + order);
		return "order number: " + order.getSerialNum();
	}
}
