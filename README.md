**deploy zipkin server**

https://hub.docker.com/r/openzipkin/zipkin/

docker pull openzipkin/zipkin

	RABBIT_ADDRESSES rabbitmq.monitor.svc:5672
	RABBIT_USER      guest 
	RABBIT_PASSWORD  guest

---

**deploy rabbit server**

https://hub.docker.com/_/rabbitmq/

docker pull rabbitmq:3.6.16-management

---

**deploy microservices**

create your own Dockerfile, i.e. replace app.jar with order-1.0.0.jar/product-1.0.0.jar/ship-1.0.0.jar (or name it as you like)

	FROM docker.io/openjdk:8-jdk-alpine
	VOLUME /tmp
	EXPOSE 8080
	COPY app.jar app.jar
	ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

---

**test microservices**

buy a product: http://{gateway_localhost}:{gateway_port}/order/buy?product={product_name}

query product status: http://{gateway_localhost}:{gateway_port}/ship/findOrder?orderNum={order_numebr}
